"1."
print("Imprimir los números del 1 al 10")
number = 1
while number <= 10:
    print(number, end=" ")
    number = number + 1

"2."
print("\n\nPedir números y sumarlos")
sum = 0
while True:
    number = int(input("Ingresa el número\t"))
    if number > 0:
        sum = sum + number
    elif number < 0:
        print("Ingresa un número válido")
    else:
        break
print(sum)



"""
3. Crea un algoritmo para imprimir el factorial de un número determinado por el usuario.
"""
# Pedir al usuario que ingrese el número
number = int(input("\nIngresa el número\t"))
# Definir las variables a utilizar
factorial = 1 # Guardar el valor del factorial
counter = number # Contador para ir multiplicando
while counter >= 1 :
    factorial = factorial * counter
    counter = counter - 1

print(f"El factorial del número es {factorial}")

