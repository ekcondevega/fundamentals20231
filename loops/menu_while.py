option = 1
while option != 0:
    print("""
    ¡Bienvenido/a! Selecciona lo que deseas hacer
    1. Calcular el factorial
    0. Salir
    """)
    option = int(input("..."))
    if option == 1:
        # Pedir al usuario que ingrese el número
        number = int(input("\nIngresa el número\t"))
        # Definir las variables auxiliares
        factorial = 1  # Guardar el valor del factorial
        counter = number  # Contador para ir multiplicando (4 x 3 x 2)
        while counter >= 1:
            factorial = factorial * counter
            counter = counter - 1
        print(f"El factorial del número es {factorial}")
    elif option == 0:
        print("Gracias por usar nuestro programa. ¡Hasta luego!")
    else:
        print("Ingresa una opción válida")