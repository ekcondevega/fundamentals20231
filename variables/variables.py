# Ejemplos de variables
year = 2023
month = "february"
day = 28

# Otros ejemplos de variables
x = 2
y = 3
x + y

"""Operadores aritméticos"""
# Sum
f = 2
sum = f + 7
print("Suma =", sum)

# Subtraction
p = 3
c = 5
subt = p - c
print("Resta =", subt)

# Multiplication
b = 1
m = -10
mult = b * m
print("Multiplicación =", mult)

# Exponentiation
x = 2
y = 7
exp = x ** y
print("Exponenciación =", exp)

# True division
x = 5
y = -2
true_div = b / m
print("División =", true_div)

# Floor division
x = 5
y = -2
floor_div = b // m
print("División =", floor_div)

# Remainder
r = 4
s = 2
rem = r % s
print("División =", rem)

"""Declaración de variables y actualización de valores"""
# Logic example 1
a = 2
b = 3
c = 1
a = b + c
b = 4
c = a + b
print("a =", a, "b =", b, "c =", c)

# Logic example 2
a = 7
b = -2
c = -10
a = a * b
b = c * -2
c = c
print("a =", a, "b =", b, "c =", c)

# Logic example 3
a = 2
b = -2
c = 1
d = a * (a + b) * c
print("a =", a, "b =", b, "c =", c, "d =", d)

"""Variables vs constants"""
variable = input("Ingresa el valor de la variable...")
constant = 3.14159

"""Tipos de datos"""
a = 10
b = 0b10
c = 4.2
d = 1.79e308
e = "hello"
f = 2 + 3j
print(f"Tipos de datos {type(a)} {type(b)} {type(c)} {type(d)} {type(e)} {type(f)}")