# Solución del problema
print("Ejercicio 1\nVamos a calcular el valor de la resistencia total")
r1 = int(input("Ingresa el valor de la resistencia R1\t"))
r2 = int(input("Ingresa el valor de la resistencia R2\t"))
r_total = 1/((1/r1)+(1/r2))

# Persistencia de la información en un archivo de texto
with open("resistance.txt", mode="w") as resistance:
    resistance.write(f"R1= {r1}\n")
    resistance.write(f"R2= {r2}\n")
    resistance.write(f"Rt= {r_total}\n")
print(f"El valor de la resistencia total de R1 = {r1} y R2 = {r2} es {r_total}")

# Lectura de la información guardada en el archivo de texto
with open("resistance.txt", mode="r") as resistance:
    for record in resistance:
        print(record)