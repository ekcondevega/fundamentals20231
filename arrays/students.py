"""
Cree un programa que permita almacenar en arreglos diferentes los nombres, los géneros y las edades de un grupo de n personas. El programa debe presentar:
	a. Cuántas personas son de género masculino
	b. Cuántas personas de género femenino superan la mayoría de edad
	c. Cuál es el promedio de edad de las personas de género masculino
	d. Cuál es el nombre de la persona de género femenino más pequeña.
"""
numero_personas = int(input("Ingresa el número de personas\t"))

nombres = []
generos = []
edades = []

for var in range(0, numero_personas):
    nombres.append(input(f"Ingresa el nombre de la persona {var+1}\t"))
    generos.append(input(f"Ingresa el género de la persona {var + 1}\t"))
    edades.append(int(input(f"Ingresa la edad de la persona {var + 1}\t")))

print(nombres)
print(generos)
print(edades)

for var in range(0, numero_personas):
    print(f"Nombre = {nombres[var]}, Género = {generos[var]}, Edad = {edades[var]}")