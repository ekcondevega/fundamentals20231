
notas_estudiantes = [[77, 68, 86, 73], [96, 87, 89, 81], [70, 90, 86, 81]]

for fila in notas_estudiantes:
    for item_col in fila:
        print(item_col, end=' ')
    print()

for i, row in enumerate(notas_estudiantes):
    for j, item in enumerate(row):
        print(f'a[{i}][{j}]={item} ', end=' ')
    print()

print()

for i in range(len(notas_estudiantes)):
    for j in range(len(notas_estudiantes[i])):
        print(f'a[{i}][{j}]={item} ', end=' ')
    print()

numeros = [[], [], []]
for i in range(0, 3):
    for j in range(0, 3):
        numeros[i].append(int(input(f'Ingrese el número de la posición [{i}][{j}]')))
print(numeros)

numeros = []
filas = int(input('Ingrese el número de filas'))
cols = int(input('Ingrese el número de columnas'))
for fila in range(filas):
    numeros.append([])
    for col in range(cols):
        num = int(input(f'Ingrese el número de la posición [{fila}][{col}]'))
        numeros[fila].append(num)
print(numeros)


