notas = [4, 5, 4, 3, 2]
print(notas)
for nota in notas:
    print(f"Nota = {nota}")

print("\n")
frutas = ["Manzana", "Pera", "Piña", "Fresa"]
for fruta in frutas:
    print(f"Fruta = {fruta}")

print("\n")
estudiantes = ["Jhan Carlos Muñoz", "Sharet Ailyn Vargas"]
for estudiante in estudiantes:
    print(f"Estudiante = {estudiante}")

print("\n")
informacion = ["Jhan Carlos Muñoz", "01/01/2000", 5, "Soltero", 64.5]
for elemento in informacion:
    print(f"{elemento}")

print("\n")
lista_rep = []
lista_rep.append("Perfect")
lista_rep.append("Confidente")
lista_rep.append(3)
lista_rep[0] = lista_rep[0] + " Enchanted"
print("Mi lista de reproducción")
for cancion in lista_rep:
    print(cancion)
print(f"Tengo {len(lista_rep)} canciones\n")

print("Mi lista de reproducción numerada")
for var in range(0, len(lista_rep)):
    print(f"{var+1}. {lista_rep[var]}")
print(f"Esta es la última canción {lista_rep[-1]}" )