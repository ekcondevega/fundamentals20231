numeros_primos = (2, 3, 5, 7, 11, 13, 17, 19) # Tupla
print(numeros_primos)
print(type(numeros_primos)) # Permite conocer el tipo de dato
# numeros_primos[0] = 1 # No soporta actualización de sus elementos
print(f"El número de la posisión 2 es = {numeros_primos[2]}")
print("Extraer los elementos de una tupla en variables")
nump1, nump2, nump3, nump4, nump5, nump6, nump7, nump8 = numeros_primos
print(nump1, nump2, nump3, nump4, nump5, nump6, nump7, nump8)
print(f"Elementos de las posiciones 2 hasta 6 = {numeros_primos[2:6]}")
print(f"Elementos de las posiciones 0 hasta 2 = {numeros_primos[:2]}")
print(f"Elementos de las posiciones 2 hasta el final de la tupla = {numeros_primos[2:]}")
