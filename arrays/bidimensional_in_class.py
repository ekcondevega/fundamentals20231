notas_estudiantes = [[77, 68, 86, 73], [96, 87, 89, 81], [70, 90, 86, 81]]
print(notas_estudiantes)

for fila in notas_estudiantes: # For para las filas
    for item_col in fila: # For para las columnas
        print(item_col, end=' ') # Print para mostrar cada elemento
    print() # Print para hacer el salto de línea al terminar cada fila

print()
nota_mayor_80 = 0
array_notas_mayor_80 = []
for indice_fila, fila in enumerate(notas_estudiantes): # For -> filas
    for indice_col, item_col in enumerate(fila): # For -> columnas
        print(f'Item[{indice_fila}][{indice_col}]={item_col} ', end=' ')
        if item_col > 80:
            nota_mayor_80 += 1
            array_notas_mayor_80.append(item_col)
    print()
print(f"{nota_mayor_80} notas mayores que 80")
print(array_notas_mayor_80)

for i in range(len(notas_estudiantes)):
    for j in range(len(notas_estudiantes[i])):
        print(f'Item[{i}][{j}]={notas_estudiantes[i][j]} ', end=' ')
    print()

