grades = [4, 5, 3.4, 3, 4.2, 1.7, 3.4]
print(grades)
print(grades.index(3.4)) # Devuelve el índice del elemento indicado
grades.sort() # Ordenar de menor a mayor
print(grades)
grades.remove(3.4) # Elimina el elemento indicado
print(grades)
grades.pop() # Elimina el último elemento o la posición indicada
print("pop", grades)
grades[0] = 1
grades2 = grades.copy()
grades[0] = 2
print(grades2)
grades.reverse() # Invierte el orden de los elementos
print(grades)
grades2.clear() # Elimina todos los elementos
print(grades2)
print(grades)
print(grades.count(4)) # Contar cuántas veces aparece un elemento
grades2 = grades.copy()
grades2.reverse()
grades.extend(grades2)
print(grades)
grades.insert(0, 3) # Insertar un elemento en una posición
print(grades)