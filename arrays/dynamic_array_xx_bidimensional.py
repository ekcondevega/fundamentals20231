numeros = [] # Creo el arreglo unidimensional vacío
filas = int(input('Ingrese el número de filas\t'))
cols = int(input('Ingrese el número de columnas\t'))
for fila in range(filas):
    numeros.append([]) # Añado cada fila vacía
    for col in range(cols):
        num = int(input(f'Ingrese el número de la posición [{fila}][{col}]\t'))
        numeros[fila].append(num)
print(numeros)

numeros = [] # Creo el arreglo unidimensional vacío
filas = int(input('Ingrese el número de filas\t'))
for fila in range(filas):
    numeros.append([]) # Añado cada fila vacía
    cols = int(input(f'Ingrese el número de columnas para la fila {fila}\t'))
    for col in range(cols):
        num = int(input(f'Ingrese el número de la posición [{fila}][{col}]\t'))
        numeros[fila].append(num)
print(numeros)