import csv

with open('grades.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    line_count = 0
    average = 0
    for row in csv_reader:
        if line_count == 0:
            print("column names")
            for elemt in row:
                print(f'{elemt}')
            line_count += 1
        else:
            average += int(row[2])
            print(f'\tName= {row[0]} LastName= {row[1]} Grade= {row[2]}.')
            line_count += 1
    print(f'Processed {line_count} lines.')
    print(average/line_count)