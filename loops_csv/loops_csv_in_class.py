import csv
with open('titiribi.csv') as archivo_csv:
    lector_csv = csv.reader(archivo_csv, delimiter=',')
    contador = 0
    contador_colegio_a = 0
    promedio_mate_colegio_a = 0
    promedio_esp_colegio_a = 0
    promedio_ing_colegio_a = 0
    promedio_gen_colegio_a = 0
    for fila in lector_csv:
        if contador == 0:
            print(f"Las cabeceras del archivo son {fila}")
        else:
            print(f"Colegio = {fila[0]}, Matemáticas = {fila[1]}, Español = {fila[2]}, Inglés = {fila[3]}")
        contador = contador + 1 # contador += 1

        if fila[0] == "A":
            contador_colegio_a += 1
            promedio_mate_colegio_a = promedio_mate_colegio_a + int(fila[1])
            promedio_esp_colegio_a = promedio_esp_colegio_a + int(fila[2])
            promedio_ing_colegio_a = promedio_ing_colegio_a + int(fila[3])

    print(f"El promedio del colegio A en Matemáticas fue = {promedio_mate_colegio_a/contador_colegio_a}")
    print(f"El promedio del colegio A en Español fue = {promedio_esp_colegio_a /contador_colegio_a}")
    print(f"El promedio del colegio A en Inglés fue = {promedio_ing_colegio_a /contador_colegio_a}")
    print(f"El promedio general del colegio A fue = {((promedio_mate_colegio_a/contador_colegio_a)+(promedio_esp_colegio_a /contador_colegio_a)+(promedio_ing_colegio_a /contador_colegio_a))/3}")