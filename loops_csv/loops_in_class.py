grades = [5, 3, 4, 5, 2, 1, 4]
counter = 0
average = 0
for grade in grades:
    print(grade, end=" ")
    if grade >= 3:
        average = average + grade
        counter = counter + 1
print(f"""
{counter} notas
Promedio = {average/counter} 
""")