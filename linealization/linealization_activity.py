# Column 1 values
a = 1
b = 2
c = 4
d = -2
v0 = 8
t = 7

# Exercises
e = (c**2 + b)/d
x = (v0*t) + (a*(t**2))/2
y = (a/(a-b))/(a/(a-b))
z = (a + b - (c/(a*d)))/(a-b*(c/d))
w = ((a/b)+(b/c))/((a/b)+(c/d))
print(f"""
e = {e}
x = {x}
y = {y}
z = {z}
w = {w}
""")

# ************************************************************************************

# Column 2 values
a = -2
b = 5
c = 2
d = 6
v0 = 0.8
t = 17

# Exercises
e = (c**2 + b)/d
x = (v0*t) + (a*(t**2))/2
y = (a/(a-b))/(a/(a-b))
z = (a + b - (c/(a*d)))/(a-b*(c/d))
w = ((a/b)+(b/c))/((a/b)+(c/d))
print(f"""
e = {e}
x = {x}
y = {y}
z = {z}
w = {w}
""")

# ************************************************************************************

# Column 3 values
a = 3
b = -2
c = 10
d = 1
v0 = 1
t = 2

# Exercises
e = (c**2 + b)/d
x = (v0*t) + (a*(t**2))/2
y = (a/(a-b))/(a/(a-b))
z = (a + b - (c/(a*d)))/(a-b*(c/d))
w = ((a/b)+(b/c))/((a/b)+(c/d))
print(f"""
e = {e}
x = {x}
y = {y}
z = {z}
w = {w}
""")


